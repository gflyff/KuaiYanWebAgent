import {createStore} from "vuex";
import {inputEmits} from "element-plus";
import {ITab} from "@/store/ITab";
import {useRouter} from "vue-router";

interface state全局状态 {
    count: number
    Token: string
    ITabList: Array<ITab>
    Tabs菜单当前Path: string
    UserInfo: object
    搜索_代理账号: object
    搜索_库存管理: object

    搜索_卡号列表: object
    搜索_余额日志: object
    搜索_库存日志: object
    搜索_制卡日志: object
    搜索_用户消息: object
    搜索_个人中心: object

}


export const store = createStore<state全局状态>({
    state() {
        return {
            count: 0,
            ITabList: [],
            Tabs菜单当前Path: "",
            Token: window.localStorage.getItem('token') || '',
            UserInfo: {
                "AgentInfo": {
                    "Id": 1,
                    "User": "",
                    "phone": "",
                    "Email": "",
                    "Qq": "",
                    "Status": 1,
                    "authority": "All"
                },
                "UserMsgNoRead": 0,
                "功能权限": [],
            },
            搜索_代理账号: {},
            搜索_库存管理: {},
            搜索_卡号列表: {},
            搜索_余额日志: {},
            搜索_库存日志: {},
            搜索_制卡日志: {},
            搜索_用户消息: {},
            搜索_个人中心: {数组_可购买充值卡:[],支付通道状态:{},订单信息:{订单ID: "", PayQRCode: "", PayURL: "", 订单状态: 0}},
        }
    },
    mutations: {
        onCountAdd(state全局状态: state全局状态) {
            state全局状态.count++
        },
        addITab(state全局状态: state全局状态, ITab: ITab) {
            const isSome = state全局状态.ITabList.some((item) => item.path == ITab.path)
            //console.log("isSome" + isSome + " path:" + ITab.path + ",Title:" + ITab.title)
            if (!isSome) {
                state全局状态.ITabList.push(ITab)
            }
        },
        DeleteITab(state全局状态: state全局状态, Path: string) {
            const isSome = state全局状态.ITabList.findIndex((item) => item.path == Path)
            state全局状态.ITabList.splice(isSome, 1)
        },
        on更新菜单当前Path(state全局状态: state全局状态, Path: string) {
            state全局状态.Tabs菜单当前Path = Path
        },
        onTabs菜单删除(state全局状态: state全局状态, 命令: string) {
            const index = state全局状态.ITabList.findIndex((item) => item.path == state全局状态.Tabs菜单当前Path)
            switch (命令) {
                case "关闭所有":
                    state全局状态.ITabList = [{path: "/应用管理/卡号列表", title: "卡号列表"}]
                    sessionStorage.removeItem("Tabs_Router")
                    break;
                case "关闭左边":
                    state全局状态.ITabList.splice(0, index)
                    break;
                case "关闭右边":
                    state全局状态.ITabList.splice(index + 1)
                    break;
                case "关闭其他":
                    state全局状态.ITabList.filter((item) => item.path == state全局状态.Tabs菜单当前Path)
                    break;
                default:
                    console.info("菜单命令信息错误")
            }
        },
        setToken(state全局状态: state全局状态, Token: string) {
            state全局状态.Token = Token
            window.localStorage.setItem('Token', Token)
        },
        setUserInfo(state全局状态: state全局状态, UserInfo: object) {
            state全局状态.UserInfo = UserInfo
            window.localStorage.setItem('UserInfo', JSON.stringify(UserInfo))
        },
        NeedInit(state全局状态: state全局状态) {
            state全局状态.Token = ''
            window.localStorage.removeItem('Token')
            localStorage.clear()

        },
        set搜索_卡号列表(state全局状态: state全局状态, data: object) {
            state全局状态.搜索_卡号列表 = data
        },
        搜索_代理账号(state全局状态: state全局状态, data: object) {
            state全局状态.搜索_代理账号 = data
        },
        set搜索_余额日志(state全局状态: state全局状态, data: object) {
            state全局状态.搜索_余额日志 = data
        },
        set搜索_制卡日志(state全局状态: state全局状态, data: object) {
            state全局状态.搜索_制卡日志 = data
        },
        set搜索_用户消息(state全局状态: state全局状态, data: object) {
            state全局状态.搜索_用户消息 = data
        },
        set搜索_个人中心(state全局状态: state全局状态, data: object) {
            state全局状态.搜索_个人中心 = data
        },
        set搜索_代理账号(state全局状态: state全局状态, data: object) {
            state全局状态.搜索_代理账号 = data
        },
        set搜索_库存管理(state全局状态: state全局状态, data: object) {
            state全局状态.搜索_库存管理 = data
        },
        set搜索_库存日志(state全局状态: state全局状态, data: object) {
            state全局状态.搜索_库存日志 = data
        },


    },
    getters: {
        getItabArray(state全局状态: state全局状态) {
            return state全局状态.ITabList
        }
    }

})

