import service from '@/api/request'

const url="/Agent/App/"

export const  GetAppIdNameList = () => {
  return service({
    url: url+'GetAppIdNameList',
    method: 'get',
  })
}
