import service from '@/api/request'

const url="/Agent/OtherFunc/"

//修改用户绑定信息
export const SetAppUserKey = (data) => {
  return service({
    url: url+'SetAppUserKey',
    method: 'post',
    data: data
  })
}
