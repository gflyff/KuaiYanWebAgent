import service from '@/api/request'

const url='Agent/Menu/'

// @Summary 获取管理员信息
export const GetAgentInfo = () => {
  return service({
    url: url+'GetAgentInfo',
    method: 'get'
  })
}

// @Summary 注销登录
export const OutLogin = () => {
  return service({
    url: url+'OutLogin',
    method: 'post'
  })
}

// @Summary 修改密码
export const AgentNewPassword = (data) => {
  return service({
    url: url+'NewPassword',
    method: 'post',
    data: data
  })
}


export const 取支付通道状态 = (data) => {
  return service({
    url:  url+'GetPayStatus',
    method: 'post',
    data: data
  })
}


export const 取余额充值地址 = (data) => {
  return service({
    url:  url+'GetPayPC',
    method: 'post',
    data: data
  })
}
export const 取余额充值订单状态地址 = (data) => {
  return service({
    url:  url+'GetPayOrderStatus',
    method: 'post',
    data: data
  })
}
