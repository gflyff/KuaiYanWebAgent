import service from '@/api/request'

const url="/Agent/Ka/"
// 分页获取Ka信息列表
//{ "Page": 0,"Size": 10 }
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
export const GetKaList = (data) => {
  return service({
    url: url+'GetList',
    method: 'post',
    data: data
  })
}

//  Del批量追回Ka
//{ "id": [ 5 ]}啊啊啊啊
export const Del批量追回Ka = (data) => {
  return service({
    url: url+'Recover',
    method: 'post',
    data: data
  })
}
//  Del批量追回Ka
//{ "id": [ 5 ]}
export const 卡号使用 = (data) => {
  return service({
    url: url+'UseKa',
    method: 'post',
    data: data
  })
}
//  Del批量追回Ka
//{ "id": [ 5 ]}
export const 卡号更换 = (data) => {
  return service({
    url: url+'ReplaceKaName',
    method: 'post',
    data: data
  })
}

//  GetKa详细信息
//{"Id": 1}
// @Success 200 {string} json "{"code": 0, "data": {},"msg": "获取成功"}"
export const GetKa详细信息 = (data) => {
  return service({
    url:  url+'GetInfo',
    method: 'post',
    data: data
  })
}

//  NewKa信息
//...
// @Success 200 {string} json "{"code": 0, "data": {},"msg": "添加成功"}"
export const  NewKa信息 = (data) => {
  return service({
    url: url+'New',
    method: 'post',
    data: data
  })
}
//  NewKa信息
//...
// @Success 200 {string} json "{"code": 0, "data": {},"msg": "添加成功"}"
export const  库存制卡 = (data) => {
  return service({
    url: url+'InventoryNewKa',
    method: 'post',
    data: data
  })
}


//  SetStatus
//...
// @Success 200 {string} json "{"code": 0, "data": {},"msg": "修改成"}"
export const  SetStatus = (data) => {
  return service({
    url: url+'SetStatus',
    method: 'post',
    data: data
  })
}

//  SetAgentNote
//...
// @Success 200 {string} json "{"code": 0, "data": {},"msg": "修改成"}"
export const SetAgentNote = (data) => {
  return service({
    url: url+'SetAgentNote',
    method: 'post',
    data: data
  })
}
export const get图表卡号统计制卡 = (data) => {
  return service({
    url: url+'ChartKaRegister',
    method: 'POST',
    donNotShowLoading: true,
    data: data
  })
}


//  取卡号格式模板配置
//...
// @Success 200 {string} json "{"code": 0, "data": {},"msg": "修改成"}"
export const SetKaTemplate = (data) => {
  return service({
    url: url+'SetKaTemplate',
    method: 'post',
    data: data
  })
}

//  取卡号格式模板配置
//...
// @Success 200 {string} json "{"code": 0, "data": {},"msg": "修改成"}"
export const GetKaTemplate = (data) => {
  return service({
    url: url+'GetKaTemplate',
    method: 'post',
    data: data
  })
}
