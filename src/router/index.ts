import {createRouter, createWebHashHistory, RouteRecordRaw,} from "vue-router";
import path from "path";
import Nprogress from 'nprogress'
// @ts-ignore
// @ts-ignore
// @ts-ignore
const routes: Array<RouteRecordRaw> = [
    {
        path: '/login',
        name: 'Login',
        component: () => import('@/view/login/login.vue')
    },
    {
        path: "/",
        name: "/",
        redirect: "/应用管理/卡号列表",
        component: () => import("@/view/layout/layout.vue"),
        children: [
            {
                path: "代理管理/代理账号",
                name: "代理账号",
                component: () => import("@/view/代理管理/代理账号.vue"),
            },
            {
                path: "代理管理/库存管理",
                name: "库存管理",
                component: () => import("@/view/代理管理/库存管理.vue"),
            },
            {
                path: "应用管理/卡号列表",
                name: "卡号列表",
                component: () => import("@/view/应用管理/卡号列表.vue"),
            },
            {
                path: "日志管理/余额日志",
                name: "余额日志",
                component: () => import("@/view/日志管理/余额日志.vue"),
            },
            {
                path: "日志管理/库存日志",
                name: "库存日志",
                component: () => import("@/view/日志管理/库存日志.vue"),
            },
            {
                path: "日志管理/制卡日志",
                name: "制卡日志",
                component: () => import("@/view/日志管理/卡号操作日志.vue"),
            },
            {
                path: "用户维护/修改绑定信息",
                name: "修改绑定信息",
                component: () => import("@/view/用户维护/修改绑定信息.vue"),
            },
            {
                path: "个人中心",
                name: "个人中心",
                component: () => import("@/view/个人中心/个人中心.vue"),
            },
        ]
    }
]

//修改为二级目录Agent下
const router = createRouter({
    history: createWebHashHistory('/Agent/#'),
    routes: routes
})

var isF = false; //这个是用于判断动态路由是否已经被获取
const 递归添加路由 = (result: any) => {
    let currenRoutes = router.options.routes
    if (result) {
        result.forEach((item: any) => {
            router.addRoute("layout", {
                path: item.path,
                name: item.name,
                meta: {
                    title: item.name,
                },
                component: () => import(`/* @vite-ignore */ @/view${item.component}`),
            });

            if (item.children && item.children.length) {
                递归添加路由(item.children);
            }
        });
    }
};
const 路由守卫白名单 = ['/Login']
//路由守卫
router.beforeEach(async (to, from) => {
    Nprogress.start()

    if ( to.path.indexOf('%') !== -1) {
        to.path= decodeURI(to.path) //中文路由坑点, 正常跳转没问题,但是刷新后路由会是url编码后 所以会找不到路由 跳404,必须解码一次才能找到正确路由,英文就没这个问题,但是我还是喜欢中文,
    }
    console.log("路由守卫:" + localStorage.getItem("Token"))
    if (to.path == "/Login") {
        return true;
    }

    //console.log(to)
    if (路由守卫白名单.indexOf(to.path) > -1) {
        return true
    }
    //如果没有 token  跳转登录
    if (!localStorage.getItem("Token")) {

        return {path: "/Login"};
    } else {
        if (isF) {
            return true;
        } else {
            //let add = Store.getters.menuList || "";
            //routerData(add);
            isF = true;
            return {...to, replace: true};
        }
    }
});


//递归添加路由(权限json)

//最后添加404路由
router.addRoute({
    path: "/:catchAll(.*)",
    component: () => import("@/view/layout/layout.vue"),
    children: [{
        path: "/:catchAll(.*)",
        component: () => import("@/view/error/index.vue")
    }]
})

// router.beforeEach((to, from, next) => {
//     next();
// })
// router.afterEach((to, from) => {
//     console.log(from)
// })
router.afterEach(() => {
    // 路由加载完成后关闭进度条
    Nprogress.done()
})

router.onError(() => {
    // 路由发生错误后销毁进度条
    Nprogress.remove()
})

export default router
